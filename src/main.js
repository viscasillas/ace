const chalk = require("chalk");
const { Curl } = require("node-libcurl");
const fs = require("fs");
const execSh = require("exec-sh");
// const { spawn } = require("child_process");
// const { exec } = require("child_process");

function aceProjectConfigurationRunner(projectName, aceConfig) {
  console.log(
    '- creating a new "' +
      aceConfig.name +
      '" project in ./' +
      projectName +
      " directory"
  );

  // TODO: CHECK "dependencies" in the config file for missing commands

  //   Lets initialize the project using the project steps
  aceConfig.initSteps.map((step, i) => {
    console.log(`-- [step ${i + 1}]: ${step.message}`);
    execSh(step.exec, { cwd: process.cwd() }, function (err) {
      if (err) {
        console.log("Exit code: ", err.code);
      }
    });
  });

  //   JSONtoFiles("src", aceConfig.srcSkeleton);
}

const LOG = (str) => console.log(str);

// Welcome our user.. its the right thing to do
LOG(chalk.blue.bold("ACE PROJECT MANAGER"));

let arg = process.argv;
let cwd = process.cwd();
let commandString = null;

// we dont really need these binaries
delete arg[0];
delete arg[1];

// Step 0: Is all strings passed after the ace command
if (arg.filter(String)) {
  commandString = arg.filter(String);
  //   LOG(commandString);
}

// Step 1: Check if ace config exists in the current working directory?
function doesAceConfigExists() {
  fs.readFile(cwd + "/ace.json", (err, data) => {
    if (err) {
      return false;
    } else {
      let aceConfig = JSON.parse(data);
      return aceConfig;
    }
  });
}

// get ace config from gitlab
function getRemoteAceConfigAndSaveToDir(aceRemoteConfigName, projectName) {
  const curl = new Curl();
  curl.setOpt(
    "URL",
    "https://gitlab.com/viscasillas/ace/-/raw/master/configs/" +
      aceRemoteConfigName +
      "/config.js?inline=false"
  );
  curl.setOpt("FOLLOWLOCATION", true);
  curl.on("end", function (statusCode, data, headers) {
    fs.writeFile(
      cwd + "/" + projectName + "/" + ".ace-config.js",
      data,
      (err) => {
        if (err) {
          console.error(err);
          return;
        }
      }
    );
    this.close();
  });
  curl.on("error", curl.close.bind(curl));
  curl.perform();
}

// ONLY FOR EXISTING ACE PROJECTS
if (doesAceConfigExists()) {
  LOG("Ace config does exists");

  //   ONLY FOR NEW ACE PROJECTS
} else {
  LOG("- ace.json does not exists, gonna try to init a project?");

  if (commandString[0] == "init" && commandString[1] && commandString[2]) {
    let projectName = commandString[1].replace(" ", "-").toLowerCase();
    let remoteAceConfigName = commandString[2];

    // is there already an ace project named with this name?
    if (fs.existsSync(cwd + "/" + projectName)) {
      LOG("- directory already exists, will not create a new ace project");
    } else {
      LOG("- initiating a new project", projectName);

      // Create a project directory
      if (!fs.existsSync(cwd + "/" + projectName)) {
        fs.mkdirSync(cwd + "/" + projectName);
      }
      // lets fetch the desired configuration and save it to the project dir
      LOG('- fetching configuration for "' + remoteAceConfigName + '"');
      getRemoteAceConfigAndSaveToDir(remoteAceConfigName, projectName);

      setTimeout(function () {
        if (fs.existsSync(cwd + "/" + projectName + "/.ace-config.js")) {
          const locallyDownloadedAceConfig = require(cwd +
            "/" +
            projectName +
            "/.ace-config.js");

          //   Now we can use the .ace-config to setup a project

          LOG("- getting into the project directory to perform some magic");
          try {
            process.chdir(cwd + "/" + projectName);
            aceProjectConfigurationRunner(
              projectName,
              locallyDownloadedAceConfig
            );
          } catch (err) {
            console.log("chdir: " + err);
          }

          //   you can do stuff outside the directory down here after
        }
      }, 2000);
    }
  } else {
    LOG(
      "- ace must be used inside of an ace project, or to init a new ace project (hint: `ace init <project-name> <config-name>`)"
    );
  }
}
