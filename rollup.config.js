import shebang from "rollup-plugin-add-shebang";
// import commonjs from "@rollup/plugin-commonjs";

export default {
  input: "src/main.js",
  output: {
    file: "./build/bundle.js",
    format: "cjs",
  },
  plugins: [
    shebang({
      // A single or an array of filename patterns. Defaults to ['**/cli.js', '**/bin.js'].
      include: "build/bundle.js",
      // you could also 'exclude' here
      // or specify a special shebang (or a function returning one) using the 'shebang' option
    }),
  ],
};
